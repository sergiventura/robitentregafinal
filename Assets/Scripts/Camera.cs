using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    //Variables
    public Transform player;
    public float smooth = 0.3f;
    public float height;
    private Vector3 velocity = Vector3.zero;
    public float x_MaxLimit;
    public float x_MinLimit;
    public float z_MinLimit;
    public float z_MaxLimit;

    //Methods
    void Update() {
        Vector3 pos = new Vector3();
        
        pos.x = player.position.x;  // inicialitzem pos.x a la posició inicial del jugador
        pos.z = player.position.z - 18; // inicialitzem la altura ver visualitzar el jugador de lluny
        pos.y = transform.position.y;
        
        transform.position = Vector3.SmoothDamp(transform.position, pos, ref velocity, smooth);
        if(transform.position.z < z_MinLimit) {
            transform.position = new Vector3(transform.position.x, transform.position.y, z_MinLimit);    
        }
        if(transform.position.z > z_MaxLimit) {
            transform.position = new Vector3(transform.position.x, transform.position.y, z_MaxLimit);    
        }
        if(transform.position.x < x_MinLimit) {
            transform.position = new Vector3(x_MinLimit, transform.position.y, transform.position.z);    
        }
        if(transform.position.x > x_MaxLimit) {
            transform.position = new Vector3(x_MaxLimit, transform.position.y, transform.position.z);    
        }
        

    }

    
}
