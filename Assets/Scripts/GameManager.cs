using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static bool anyWarning;
    public Text warningsText;

    public GameObject client1;
    public GameObject client2;
    public GameObject client3;
    public GameObject client4;
    public GameObject client5;
    public GameObject client6;

    public GameObject director;
    public GameObject worker1;
    public GameObject worker2;
    public GameObject worker3;

    public GameObject guard1;
    public GameObject guard2;

    // Start is called before the first frame update
    void Start()
    {
            anyWarning = false;
            int i = Random.Range(0,3); 
            Debug.Log(i);

        if(i == 0) 
        {
            //posicionamiento por defecto
        }
        else if(i == 1) 
        {
            client1.transform.position = new Vector3(14.07f, 0f, -4.65f); 
            client2.transform.position = new Vector3(-9.52f, 0f, 19.82f);
            client3.transform.position = new Vector3(-11.11f, 0f, 16.06f);
            client4.transform.position = new Vector3(-3.85f, 0f, 2.88f);
            client5.transform.position = new Vector3(24.02f, 0f, 18.44f);
            client6.transform.position = new Vector3(24.57f, 0f, 8.42f);

            director.transform.position = new Vector3(13.05f, 0f, 14.51f);
            worker1.transform.position = new Vector3(10f, 0f, 12f);
            worker2.transform.position = new Vector3(0.3f, 0f, 6.3f);  
            worker3.transform.position = new Vector3(23.34f, 0f, 21.62f);  

            guard1.transform.position = new Vector3(8.28f, 0f, 6.65f);  
            guard2.transform.position = new Vector3(5.77f, 0f, -8.03f);  

        }
        else
        {
            client1.transform.position = new Vector3(24.05f, 0f, 3.48f); 
            client2.transform.position = new Vector3(-9.52f, 0f, 19.82f);
            client3.transform.position = new Vector3(-11.11f, 0f, 16.06f);
            client4.transform.position = new Vector3(-7.33f, 0f, 12.49f);
            client5.transform.position = new Vector3(16.37f, 0f, 17.8f);
            client6.transform.position = new Vector3(25.5f, 0f, 3.46f);

            director.transform.position = new Vector3(1f, 0f, 3f);
            worker1.transform.position = new Vector3(10f, 0f, 12f);
            worker2.transform.position = new Vector3(20f, 0f, 0f);  
            worker3.transform.position = new Vector3(14.94f, 0f, 21.62f);  

            guard1.transform.position = new Vector3(8.28f, 0f, 6.65f);  
            guard2.transform.position = new Vector3(17.6f, 0f, 3.25f);  
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!anyWarning) 
        {
            warningsText.text = "";  //Para borrar warning cuando ya no hay peligro         
        }   
    }
}
