using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmeryProgressBar : MonoBehaviour
{
    public Image fill;
    public GameObject progressBar;
    public float secondsToOpen;
    public static float seconds;
    public static bool finished;

    // Start is called before the first frame update
    void Start()
    {
        finished = false;
        seconds = 0;
        progressBar.SetActive(false);     
    }

    // Update is called once per frame
    void Update()
    {
        seconds += 1*Time.deltaTime;
        fill.fillAmount = seconds/secondsToOpen;  
        if(seconds >= secondsToOpen) 
        {
            finished = true;
        }  
    }
    public static void Reset() {
        finished = false;
        seconds = 0;     
    }
}
