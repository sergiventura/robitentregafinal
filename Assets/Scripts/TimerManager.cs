using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerManager : MonoBehaviour
{
    public Image fill;
    public GameObject timer;
    public Text timerText;
    public float maxSeconds;
    private float seconds;
    public Text warningsText;
    public static bool policeComing;
    public AudioSource alarmAudio;
    private bool started;
    
    // Start is called before the first frame update
    void Start()
    {
        seconds = maxSeconds;
        timer.SetActive(false); 
        policeComing = false;   
        started = false; 
        GameManager.anyWarning = false;     
    }

    // Update is called once per frame
    void Update()
    {
        if(policeComing && !GameOverManager.gameEnded)  
        {
            timer.SetActive(true); 
            warningsText.text = "Police Coming";
            if(seconds >= 0)
            {
                seconds -= 1*Time.deltaTime;
                UpdateUI(seconds);
                if (seconds <= 0)
                {
                    GameOverManager.police = true;
                }
            }
            if(!started) 
            {
                alarmAudio.Play();  
                started = true;
            }
           
        }
        if(GameOverManager.gameEnded)
        {
            alarmAudio.Pause();
        }
                 
    }

    private void UpdateUI(float seconds) 
    {
        timerText.text = string.Format("{0:D2}:{1:D2}", (int)seconds/60, (int)seconds%60);
        fill.fillAmount = seconds/maxSeconds;
    }
}
