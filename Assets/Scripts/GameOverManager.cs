using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOverManager : MonoBehaviour
{
    public TextMeshProUGUI deadText;
    public TextMeshProUGUI policeText;
    public TextMeshProUGUI exitText;
    public float waitTime;
    private float currentTime;
    public static bool dead;
    public static bool police;
    public static bool exitBanc;

    //Score variables
    public GameObject scorePanel;
    public AudioSource bankAudio;
    public AudioSource alarmAudio;
    private int moneyValue;
    public static int deadHostatgeValue;
    private int deadHostageScore;
    public static bool policeCalled;
    private int policeScore;
    public Text moneyScoreText;
    public Text hostageValueText;
    public Text hostatgeScoreText;
    public Text PoliceCalledText;
    public Text NoPoliceCalledText;
    public Text PoliceScoreText;
    public Text totalScoreText;
    public GameObject DeadLineText;
    public static bool gameEnded;


    void Start()
    {
        Time.timeScale = 1f;
        deadText.enabled = false;  
        policeText.enabled = false; 
        exitText.enabled = false;
        currentTime = 0;
        dead = false;
        police = false; 
        exitBanc = false;
        deadHostatgeValue = 0; 
        policeCalled = false;
        DeadLineText.SetActive(true);
        gameEnded = false;
    }

    // Update is called once per frame
    void Update() {
        if (dead || police || exitBanc) {
            gameEnded = true;
            if (currentTime < waitTime) {
                currentTime += 1*Time.deltaTime;
            }
            else {
                SetScore();   
            }
        }
        if(dead) 
        {
            deadText.enabled = true;   
        }
        else if(police)
        {
            policeText.enabled = true;    
        }
        if(exitBanc) 
        {
            exitText.enabled = true;
            exitText.text = "You Robbed " + StealMoney.moneyPercentage + " % of the money!";
        }
    }
    public void SetScore() 
    {
        Time.timeScale = 0f;
        bankAudio.Pause();
        alarmAudio.Pause();
        scorePanel.SetActive(true);

        if(Player.alive)
        {
            DeadLineText.SetActive(false);
            moneyValue = StealMoney.moneyPercentage;
            
        }
        else
        {
            DeadLineText.SetActive(true);
            moneyValue = 0;
        }
        moneyScoreText.text = moneyValue + " pts";
        if(moneyValue == 0) 
        {
            moneyScoreText.color = Color.yellow; 
            PoliceScoreText.color = Color.yellow;  
        }

        hostageValueText.text = deadHostatgeValue + "";
        

        if(deadHostatgeValue == 0) 
        {
            deadHostageScore = 0;
            hostatgeScoreText.color = Color.yellow;
        }
        else
        {
            deadHostageScore = -10*((deadHostatgeValue*(deadHostatgeValue + 1))/2);
        }

        hostatgeScoreText.text = deadHostageScore + " pts";

        if(policeCalled) 
        {
            policeScore = 0;
            PoliceCalledText.enabled = true;
            NoPoliceCalledText.enabled = false;
            PoliceScoreText.enabled = true;
            PoliceScoreText.color = Color.yellow;
        }
        else
        {
            PoliceScoreText.enabled = true;
            policeScore = moneyValue/2;
            PoliceCalledText.enabled = false;
            NoPoliceCalledText.enabled = true;
        }
        PoliceScoreText.text = policeScore + " pts";
        int totalscore;
        totalscore = (moneyValue + policeScore + deadHostageScore);
        if(!Player.alive)
        {
           totalscore -= 100;
        }

        totalScoreText.text = totalscore + " pts";
        
        if(totalscore < 0)
        {
            totalScoreText.color = Color.red;
        }
        else 
        {
            totalScoreText.color = Color.green;  
        }
    }
}
