using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CallingBar : MonoBehaviour
{
    public float maxTime;
    private float currentTime;
    public Image Fill;
    private Quaternion rotation;
    public GameObject mainCamera;
    public GameObject exclamationIcon;
    
    // Start is called before the first frame update
    void Start()
    {
        currentTime = 0;
        rotation = mainCamera.transform.rotation;  
        //callingBar.SetActive(false);     
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += 1*Time.deltaTime;
        Fill.fillAmount = currentTime/maxTime;

        if(currentTime >= maxTime) 
        {
            currentTime = 0;
            this.gameObject.SetActive(false);
            TimerManager.policeComing = true;
            exclamationIcon.SetActive(false);
            GameOverManager.policeCalled = true;

        }
    } 
    void LateUpdate() 
    {
        transform.rotation = rotation;
    }
    public void StopCall() 
    {
        currentTime = 0;
    }
}     
    
    
