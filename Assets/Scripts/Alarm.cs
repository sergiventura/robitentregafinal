using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : MonoBehaviour
{
    
    
    
    private void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "Worker") 
        {
            Player.ableToEscape = true; //permetem que surti quan s'ha avisat a la policía, no abans 
            TimerManager.policeComing = true;
            GameOverManager.policeCalled = true;
        }
    }    
}
