using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StealMoney : MonoBehaviour
{
    public static bool robantDiners;
    public Text moneyText;
    public float maxMoney;
    private float stolenMoney;
    public static int moneyPercentage;
    public GameObject exclamacioCaixaForta;
    public GameObject exclamacioSortida;
    public Text objectiveText;
    public GameObject arrowImage;

    void Start()
    {
        stolenMoney = 0;
        robantDiners = false; 
        moneyPercentage = 0;  
        exclamacioSortida.SetActive(false);
        exclamacioCaixaForta.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
        if(robantDiners && stolenMoney < maxMoney && Player.alive) {
            stolenMoney += 1*Time.deltaTime;
            arrowImage.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            arrowImage.GetComponent<Image>().color = new Color(1, 1, 0, 1);
        }
        else 
        {
            arrowImage.transform.localScale = new Vector3(1f, 1f, 1f);
            arrowImage.GetComponent<Image>().color  = new Color(1, 1, 1, 1);
        }
        moneyPercentage = (int)(100*(stolenMoney/maxMoney));
        moneyText.text = moneyPercentage  + " %";

        if(stolenMoney >= maxMoney) 
        {
            exclamacioCaixaForta.SetActive(false); 
            objectiveText.text = "Exit banc";   
        }
    }
}
