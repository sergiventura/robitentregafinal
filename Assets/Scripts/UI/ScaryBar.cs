using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaryBar : MonoBehaviour
{
    public float maxTime;
    private float currentTime;
    public Image Fill;
    public Text barText;
    public GameObject scaryBar;
    private Quaternion rotation;
    public GameObject enemy;
    public GameObject mainCamera;


    // Start is called before the first frame update
    void Start()
    {
        SetMaxTime();
        currentTime = maxTime;
        rotation = mainCamera.transform.rotation;       
    }

    // Update is called once per frame
    void Update()
    {
        if(currentTime > 0) {
            scaryBar.SetActive(true);
            currentTime -= 1*Time.deltaTime;
            UpdateBar();  
        }
        
        else {
            enemy.GetComponentInChildren<EnemyAI>().currentState.GoToStandardState();
            //this.enabled = false;
            scaryBar.SetActive(false);
            //barText.enabled = false; //no el canvio a true mai de moment
        } 

        if(currentTime <= 5 && currentTime > 0) 
        {
            scaryBar.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            Fill.color = new Color(1, 0, 0, 1);
            barText.color = new Color(1, 0, 0, 1);
        } 
        else if(currentTime > 5) 
        {
            scaryBar.transform.localScale = new Vector3(1f, 1f, 1f);
            Fill.color = new Color(0, 165/255f, 1, 1);
            barText.color = new Color(0, 165/255f, 1, 1);
        }        
    }
    void LateUpdate () 
    {
        transform.rotation = rotation;    
    }

    public void SetMaxTime() 
    {
        currentTime = maxTime;
        scaryBar.SetActive(true);
    }
    public void UpdateBar() 
    {
        barText.text = ((int)currentTime).ToString();
        Fill.fillAmount = currentTime/maxTime;
    }
}
