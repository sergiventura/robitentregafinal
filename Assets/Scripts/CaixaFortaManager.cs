using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaixaFortaManager : MonoBehaviour
{
    public Image fill;
    public GameObject leftDoor;
    public GameObject rightDoor;
    public GameObject progressBar;
    public float secondsToOpen;
    private float seconds;
    public static bool openDoor;
    private static bool obrint;
    public GameObject exclamacioPorta;
    public GameObject exclamacioCaixaForta;
    public GameObject exclamacioSortida;
    public Text objectiveText;
    public AudioSource openVault;
    public AudioSource openingVault;

    // Start is called before the first frame update
    void Start()
    {
        openDoor = false;
        obrint = false;
        seconds = 0; 
        progressBar.SetActive(false); 
        exclamacioPorta.SetActive(true); 
        exclamacioCaixaForta.SetActive(false); 
        exclamacioSortida.SetActive(false); 
          
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Space) && seconds < secondsToOpen)
        {
            obrint = true;
        } 

        if(openDoor && obrint) //si player fa trigger al collider
        { 
            openingVault.Play();    
            progressBar.SetActive(true); 
            seconds += 1*Time.deltaTime;
            fill.fillAmount = seconds/secondsToOpen;
            if(seconds >= secondsToOpen) //quant ha passat el temps
            {
                openingVault.Pause();
                openVault.Play();
                objectiveText.text = "Enter vault to steal money\nExit banc whenever you want";
                progressBar.SetActive(false);
                leftDoor.SetActive(false);
                rightDoor.SetActive(false);
                exclamacioPorta.SetActive(false); 
                exclamacioCaixaForta.SetActive(true); 
                exclamacioSortida.SetActive(true); 
                obrint = false;
                Player.ableToEscape = true;
            }
        }
        else {
            obrint = false; // per haver de clicar space si es surt de la porta
        }  
        if(!obrint) 
        {
            openingVault.Pause(); 
        }            
    }
}
