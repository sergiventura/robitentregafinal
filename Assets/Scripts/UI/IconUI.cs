using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconUI : MonoBehaviour
{
    public Image img;
    public Transform target;

    // Update is called once per frame
    void Update()
    {
        float minX = img.GetPixelAdjustedRect().width/2;  
        float maxX = Screen.width - minX;  

        float minY = img.GetPixelAdjustedRect().height/2;  
        float maxY = Screen.height - minY;  

        Vector2 pos = UnityEngine.Camera.main.WorldToScreenPoint(target.position);
        
        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);

        img.transform.position = pos;
    }
}
