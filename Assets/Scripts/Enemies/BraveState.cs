using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BraveState : IEnemyState
{
    EnemyAI myEnemy;
    
    // Cuando llamamos al constructor, guardamos 
    // una referencia a la IA de nuestro enemigo
    public BraveState(EnemyAI enemy)
    {
        myEnemy = enemy;
    }

    // Aquí va toda la funcionalidad que queramos 
    // que haga el enemigo cuando esté en este estado.
    public void UpdateState()
    {

        if(myEnemy.health <= 0)
        {
            Die();
        }
        if(myEnemy.isGuard && Player.alive)
        {
            if(myEnemy.hasObject) {
                //rotar hacia el player 
                myEnemy.transform.LookAt(myEnemy.player.transform);
                //Saber si guardia ve al jugador
                if(Physics.Raycast(myEnemy.rayOrigin.transform.position, myEnemy.transform.forward, out RaycastHit hit, 30f ))
                { 
                    Debug.DrawRay(myEnemy.rayOrigin.transform.position, myEnemy.transform.forward  * 30f, Color.green);

                    if(hit.collider.tag == "Player") {


                        //Empezar la animación de posición de disparo
                        if(myEnemy.currentTime >= myEnemy.ShootTiming - 1)
                        {
                            myEnemy.animator.SetBool("shooting", true);
                        }
                        
                        //Disparar si ha pasado el tiempo entre balas
                        if(myEnemy.currentTime >= myEnemy.ShootTiming)
                        {
                            myEnemy.currentTime = 0;
                            Shoot();
                        }
                        //evitar que se mueva si ya puede disparar
                        myEnemy.navMeshAgent.isStopped = true;
                        myEnemy.currentTime += Time.deltaTime;
                    }
                    
                    // Si no lo ve, va a por él
                    else {
                        GameManager.anyWarning = true;
                        myEnemy.warningsText.text = "Guard moving";
                        myEnemy.currentTime = myEnemy.ShootTiming - 1;
                        myEnemy.animator.SetBool("shooting", false);
                        myEnemy.agent.destination = myEnemy.player.transform.position; 
                        myEnemy.navMeshAgent.isStopped = false;   
                    }
                    
                }
                
            } 
            else
            {
                GameManager.anyWarning = true;
                myEnemy.warningsText.text = "Guard moving";
                myEnemy.agent.destination = myEnemy.armery.transform.position; 
                myEnemy.navMeshAgent.isStopped = false;
            }
            //Guard va a armería a agafar arma nova
            if(ArmeryProgressBar.finished) 
                {
                    myEnemy.animator.SetBool("armery", false);
                    myEnemy.hasObject = true;
                    myEnemy.enemyObject.SetActive(true);
                    ArmeryProgressBar.Reset(); 
                    myEnemy.armeryProgressBar.SetActive(false); 
                }
            
        } 
        //Worker va a pulsar alarma más cercana
        if(myEnemy.isWorker && !TimerManager.policeComing) 
        {
            GameManager.anyWarning = true;
            myEnemy.warningsText.text = "Worker moving";
            var nearestDist = float.MaxValue;
            Transform nearestObj = null;
            //Detectar la alarma que se encuentre más cerca
            foreach(var alarm in myEnemy.alarms) 
            {
                if(Vector3.Distance(myEnemy.transform.position, alarm.transform.position) < nearestDist) 
                {
                    nearestDist = Vector3.Distance(myEnemy.transform.position, alarm.transform.position);
                    nearestObj = alarm; 
                }
            }
            myEnemy.agent.destination = nearestObj.transform.position;
            
        }
        if (myEnemy.isWorker && TimerManager.policeComing) {
            myEnemy.exclamationIcon.SetActive(false);
            myEnemy.navMeshAgent.isStopped = true;
            myEnemy.animator.SetBool("scared", true); //no cal que apretin la alarma si ja està apretada
        }

        if(myEnemy.isClient && !TimerManager.policeComing && myEnemy.hasObject) 
        {
            myEnemy.callingBar.SetActive(true);
            GameManager.anyWarning = true;
            myEnemy.warningsText.text = "Client calling police";
        }
        if(myEnemy.isClient && TimerManager.policeComing) 
        {
            myEnemy.exclamationIcon.SetActive(false);
            myEnemy.callingBar.SetActive(false);
        }
    }
    
    public void GoToScaryState()
    {
        GameManager.anyWarning = false;
        myEnemy.animator.SetBool("scared", true);
        myEnemy.animator.SetBool("shooting", false);
        //paramos su movimiento
        
        if(myEnemy.isGuard || myEnemy.isWorker)
        {
            myEnemy.navMeshAgent.isStopped = true;
        }
        
        
        if(myEnemy.isClient) 
        {
            myEnemy.callingBarScript.StopCall();
            myEnemy.callingBar.SetActive(false);
        }

        myEnemy.currentState = myEnemy.scaryState;
    }

    
    public void GoToStandardState() 
    { 
        myEnemy.currentState = myEnemy.standardState;    
    }

    // Como ya estamos en el estado Brave, no
    // llamaremos nunca a esta función desde 
    // este estado
    public void GoToBraveState() { }
    public void Die() {
        GameManager.anyWarning = false;
        GameObject.Destroy(myEnemy.gameObject);
        GameOverManager.deadHostatgeValue++;
    }
    public void ScareEnemy() 
    {
        if(myEnemy.isGuard && myEnemy.hasObject)
        {
            //no lo asustamos porque ya nos está disparando. 
        }
        else
        {
            myEnemy.scaryBar.SetActive(true);
            myEnemy.scaryBarScript.SetMaxTime();
            myEnemy.exclamationIcon.SetActive(false);  
            GoToScaryState();
        }    
    }
    public void Shoot()
    {
        myEnemy.fireSound.Play();
        myEnemy.muzzleFlash.Play();
        GameObject.Instantiate(myEnemy.bullet.transform, myEnemy.bulletSpawnPoint.transform.position, myEnemy.transform.rotation);
        myEnemy.exclamationIcon.SetActive(false);       
    }
    
    public void OnTriggerEnter(Collider col)
    {
        //player es troba a la armeria
        if(col.tag == "Armeria") 
        {
            if(myEnemy.isGuard)
            {
                myEnemy.animator.SetBool("armery", true);
                myEnemy.armeryProgressBar.SetActive(true);     
            }        
        }   
    }
    public void OnTriggerStay(Collider col) 
    { 
        
    }
    public void OnTriggerExit(Collider col) 
    { 

    }
}